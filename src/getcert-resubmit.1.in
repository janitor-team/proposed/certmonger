.TH CERTMONGER 1 "February 9, 2015" "certmonger Manual"

.SH NAME
getcert

.SH SYNOPSIS
getcert resubmit [options]

.SH DESCRIPTION
Tells \fIcertmonger\fR to generate (or regenerate) a signing request and
submit (or resubmit) the signing request to a CA for signing.

.SH SPECIFYING REQUESTS BY NICKNAME
.TP
\fB\-i\fR \fINAME\fR, \fB\-\-id\fR=\fINAME\fR
Resubmit a signing request for the tracking request which has this nickname.
If this option is not specified, and a tracking entry which matches the key
and certificate storage options which are specified already exists, that entry
will be used.  If not specified, the location of the certificate should be
specified with either a combination of the \fB\-d\fR and \fB\-n\fR options, or
with the \fB\-f\fR option.

.SH SPECIFYING REQUESTS BY CERTIFICATE LOCATION
.TP
\fB\-d\fR \fIDIR\fR, \fR\-\-dbdir\fR=\fIDIR\fR
The certificate is in the NSS database in the specified directory.
.TP
\fB\-n\fR \fINAME\fR, \fR\-\-nickname\fR=\fINAME\fR
The certificate in the NSS database named with \fB\-d\fR has the specified
nickname.  Only valid with \fB\-d\fR.
.TP
\fB\-t\fR \fITOKEN\fR, \fB\-\-token\fR=\fITOKEN\fR
If the NSS database has more than one token available, the certificate
is stored in this token.  This argument only rarely needs to be specified.
Only valid with \fB\-d\fR.
.TP
\fB\-f\fR \fIFILE\fR, \fB\-\-certfile\fR=\fIFILE\fR
The certificate is stored in the named file.

.SH ENROLLMENT OPTIONS
.TP
\fB\-c\fR \fINAME\fR, \fB\-\-ca\fR=\fINAME\fR
Submit the new signing request to the specified CA rather than the one which
was previously associated with this certificate.  The name of
the CA should correspond to one listed by \fIgetcert list\-cas\fR.
.TP
\fB\-T\fR \fINAME\fR, \fB\-\-profile\fR=\fINAME\fR
Request a certificate using the named profile, template, or certtype,
from the specified CA.
.TP
\fB\-\-ms\-template\-spec\fR \fISPEC\fR
Include a V2 Certificate Template extension in the signing request.
This datum includes an Object Identifier, a major version number
(positive integer) and an optional minor version number.  The format
is: \fB<oid>:<majorVersion>[:<minorVersion>]\fR.
.TP
\fB\-X\fR \fINAME\fR, \fB\-\-issuer\fR=\fINAME\fR
Request a certificate using the named issuer from the specified CA.
.TP
\fB\-I\fR \fINAME\fR, \fB\-\-id\fR=\fINAME\fR
Assign the specified nickname to this task, replacing the previous nickname.

.SH SIGNING REQUEST OPTIONS
.TP
\fB\-N\fR \fINAME\fR, \fB\-\-subject\-name\fR=\fINAME\fR
Change the subject name to include in the signing request.
.TP
\fB\-u\fR \fIkeyUsage\fR, \fB\-\-key\-usage\fR=\fIkeyUsage\fR
Add an extensionRequest for the specified keyUsage to the
signing request.  The keyUsage value is expected to be one of these names:

digitalSignature

nonRepudiation

keyEncipherment

dataEncipherment

keyAgreement

keyCertSign

cRLSign

encipherOnly

decipherOnly
.TP
\fB\-U\fR \fIEKU\fR, \fB\-\-extended\-key\-usage\fR=\fIEKU\fR
Change the extendedKeyUsage value specified in an extendedKeyUsage
extension part of the extensionRequest attribute in the signing
request.  The EKU value is expected to be an object identifier (OID).
.TP
\fB\-K\fR \fINAME\fR, \fB\-\-principal\fR=\fINAME\fR
Change the Kerberos principal name specified as part of a subjectAltName
extension part of the extensionRequest attribute in the signing request.
.TP
\fB\-E\fR \fIEMAIL\fR, \fB\-\-email\fR=\fIEMAIL\fR
Change the email address specified as part of a subjectAltName
extension part of the extensionRequest attribute in the signing request.
.TP
\fB\-D\fR \fIDNSNAME\fR, \fB\-\-dns\fR=\fIDNSNAME\fR
Change the DNS name specified as part of a subjectAltName extension part of the
extensionRequest attribute in the signing request.
.TP
\fB\-A\fR \fIADDRESS\fR, \fB\-\-ip\-address\fR=\fIADDRESS\fR
Change the IP address specified as part of a subjectAltName extension part of
the extensionRequest attribute in the signing request.
.TP
\fB\-l\fR \fIFILE\fR, \fB\-\-challenge\-password\-file\fR=\fIFILE\fR
Add an optional ChallengePassword value, read from the file, to the signing
request.  A ChallengePassword is often required when the CA is accessed using
SCEP.
.TP
\fB\-L\fR \fIPIN\fR, \fB\-\-challenge\-password\fR=\fIPIN\fR
Add the argument value to the signing request as a ChallengePassword attribute.
A ChallengePassword is often required when the CA is accessed using SCEP.

.SH OTHER OPTIONS
.TP
\fB\-B\fR \fICOMMAND\fR, \fB\-\-before\-command\fR=\fICOMMAND\fR
When ever the certificate or the CA's certificates are saved to the
specified locations, run the specified command as the client user before
saving the certificates.
.TP
\fB\-C\fR \fICOMMAND\fR, \fB\-\-after\-command\fR=\fICOMMAND\fR
When ever the certificate or the CA's certificates are saved to the
specified locations, run the specified command as the client user after
saving the certificates.
.TP
\fB\-a\fR \fIDIR\fR, \fB\-\-ca\-dbdir\fR=\fIDIR\fR
When ever the certificate is saved to the specified location, if root
certificates for the CA are available, save them to the specified NSS database.
.TP
\fB\-F\fR \fIFILE\fR, \fB\-\-ca\-file\fR=\fIFILE\fR
When ever the certificate is saved to the specified location, if root
certificates for the CA are available, and when the local copies of the
CA's root certificates are updated, save them to the specified file.
.TP
\fB\-\-for\-ca\fR
Request a CA certificate.
.TP
\fB\-\-not\-for\-ca\fR
Request a non\-CA certificate (the default).
.TP
\fB\-\-ca\-path\-length\fR=\fILENGTH\fR
Path length for CA certificate. Only valid with \-\-for\-ca.
.TP
\fB\-w\fR, \fB\-\-wait\fR
Wait for the certificate to be reissued and saved, or for the attempt to obtain
one to fail.
.TP
\fB\-\-wait\-timeout\fR=\fITIMEOUT\fR
Maximum time to wait for the certificate to be issued.
.TP
\fB\-v\fR, \fB\-\-verbose\fR
Be verbose about errors.  Normally, the details of an error received from
the daemon will be suppressed if the client can make a diagnostic suggestion.
\fB\-o\fR \fIOWNER\fR, \fB\-\-key\-owner\fR=\fIOWNER\fR
After generation set the owner on the private key file or database to OWNER.
\fB\-m\fR \fIMODE\fR, \fB\-\-key\-perms\fR=\fIMODE\fR
After generation set the file permissions on the private key file or database to MODE.
\fB\-O\fR \fIOWNER\fR, \fB\-\-cert\-owner\fR=\fIOWNER\fR
After generation set the owner on the certificate file or database to OWNER.
\fB\-M\fR \fIMODE\fR, \fB\-\-cert\-perms\fR=\fIMODE\fR
After generation set the file permissions on the certificate file or database to MODE.

.SH BUGS
Please file tickets for any that you find at https://fedorahosted.org/certmonger/

.SH SEE ALSO
\fBcertmonger\fR(8)
\fBgetcert\fR(1)
\fBgetcert\-add\-ca\fR(1)
\fBgetcert\-add\-scep\-ca\fR(1)
\fBgetcert\-list\-cas\fR(1)
\fBgetcert\-list\fR(1)
\fBgetcert\-modify\-ca\fR(1)
\fBgetcert\-refresh\-ca\fR(1)
\fBgetcert\-refresh\fR(1)
\fBgetcert\-rekey\fR(1)
\fBgetcert\-remove\-ca\fR(1)
\fBgetcert\-request\fR(1)
\fBgetcert\-start\-tracking\fR(1)
\fBgetcert\-status\fR(1)
\fBgetcert\-stop\-tracking\fR(1)
\fBcertmonger\-certmaster\-submit\fR(8)
\fBcertmonger\-dogtag\-ipa\-renew\-agent\-submit\fR(8)
\fBcertmonger\-dogtag\-submit\fR(8)
\fBcertmonger\-ipa\-submit\fR(8)
\fBcertmonger\-local\-submit\fR(8)
\fBcertmonger\-scep\-submit\fR(8)
\fBcertmonger_selinux\fR(8)
