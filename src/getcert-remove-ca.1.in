.TH CERTMONGER 1 "February 24, 2015" "certmonger Manual"

.SH NAME
getcert

.SH SYNOPSIS
getcert remove\-ca [options]

.SH DESCRIPTION
Remove a CA configuration from \fIcertmonger\fR.  Enrollment requests which
reference the CA will behave as though they have no assigned CA.

.SH OPTIONS
.TP
\fB\-c\fR \fINAME\fR, \fB\-\-ca\fR=\fINAME\fR
The nickname of the CA configuration to remove.
.TP
\fB\-v\fR, \fB\-\-verbose\fR
Be verbose about errors.  Normally, the details of an error received from
the daemon will be suppressed if the client can make a diagnostic suggestion.

.SH BUGS
Please file tickets for any that you find at https://fedorahosted.org/certmonger/

.SH SEE ALSO
\fBcertmonger\fR(8)
\fBgetcert\fR(1)
\fBgetcert\-add\-ca\fR(1)
\fBgetcert\-add\-scep\-ca\fR(1)
\fBgetcert\-list\-cas\fR(1)
\fBgetcert\-list\fR(1)
\fBgetcert\-modify\-ca\fR(1)
\fBgetcert\-refresh\-ca\fR(1)
\fBgetcert\-refresh\fR(1)
\fBgetcert\-rekey\fR(1)
\fBgetcert\-request\fR(1)
\fBgetcert\-resubmit\fR(1)
\fBgetcert\-status\fR(1)
\fBgetcert\-stop\-tracking\fR(1)
\fBcertmonger\-certmaster\-submit\fR(8)
\fBcertmonger\-dogtag\-ipa\-renew\-agent\-submit\fR(8)
\fBcertmonger\-dogtag\-submit\fR(8)
\fBcertmonger\-ipa\-submit\fR(8)
\fBcertmonger\-local\-submit\fR(8)
\fBcertmonger\-scep\-submit\fR(8)
\fBcertmonger_selinux\fR(8)
