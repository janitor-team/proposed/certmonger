#!/bin/bash -e

cd "$tmpdir"

source "$srcdir"/functions
initnssdb "$tmpdir"

grep -v ^validity_period $CERTMONGER_CONFIG_DIR/certmonger.conf > \
	$tmpdir/certmonger.conf
cat >> $tmpdir/certmonger.conf << EOF
[selfsign]
validity_period = 46129s
EOF

function append() {
	cat >> $1 <<- EOF
	template_subject=CN=Babs Jensen
	template_hostname=localhost,localhost.localdomain
	template_email=root@localhost,root@localhost.localdomain
	template_principal=root@EXAMPLE.COM,root@FOO.EXAMPLE.COM
	template_ku=111
	template_eku=id-kp-clientAuth,id-kp-emailProtection
	EOF
}

function setupca() {
	cat > ca.self <<- EOF
	id=self_signer
	ca_is_default=0
	ca_type=INTERNAL:SELF
	ca_internal_serial=04
	ca_internal_issue_time=40271
	EOF
}

size=secp256r1
# Build a self-signed certificate.
run_certutil -d "$tmpdir" -S -n keyi$size \
	-s "cn=T$size" -c "cn=T$size" \
	-x -t u -k ec -q $size
# Export the certificate and key.
pk12util -C AES-128-CBC -c AES-128-CBC -d "$tmpdir" -o $size.p12 -W "" -n "keyi$size" > /dev/null 2>&1
openssl pkcs12 -in $size.p12 -passin pass: -out key.$size -nodes > /dev/null 2>&1
# Read that OpenSSL key.
cat > entry.$size <<- EOF
key_storage_type=FILE
key_storage_location=$tmpdir/key.$size
EOF
$toolsdir/keyiread entry.$size > /dev/null 2>&1
grep ^key_pubkey_info= entry.$size > pubkey.$size
grep ^key_pubkey= entry.$size >> pubkey.$size
# Use that NSS key.
cat > entry.$size <<- EOF
key_storage_type=NSSDB
key_storage_location=$tmpdir
key_nickname=keyi$size
EOF
append entry.$size
cat pubkey.$size >> entry.$size
$toolsdir/csrgen entry.$size > csr.nss.$size
setupca
$toolsdir/submit ca.self entry.$size > cert.nss.$size
# Use that OpenSSL key.
cat > entry.$size <<- EOF
key_storage_type=FILE
key_storage_location=$tmpdir/key.$size
EOF
append entry.$size
cat pubkey.$size >> entry.$size
$toolsdir/csrgen entry.$size > csr.openssl.$size
setupca
$toolsdir/submit ca.self entry.$size > cert.openssl.$size
# Now check their signatures.
grep -v CERTIFICATE cert.nss.$size     | base64 -d | $toolsdir/checksig
grep -v CERTIFICATE cert.openssl.$size | base64 -d | $toolsdir/checksig
echo Test complete.
