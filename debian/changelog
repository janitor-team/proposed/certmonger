certmonger (0.79.16-1) unstable; urgency=medium

  * New upstream release. (LP: #1987276)

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 26 Aug 2022 09:42:54 +0300

certmonger (0.79.15-1) unstable; urgency=medium

  * New upstream release. (Closes: #1003750)
  * control: Bump debhelper-compat to 13, policy to 4.6.0.
  * 0001-candidate-openssl-3.0-compat-fixes.patch: Dropped, upstream.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 23 Feb 2022 13:48:49 +0200

certmonger (0.79.14+git20211010-3) experimental; urgency=medium

  * Fix build with OpenSSL 3.0. (Closes: #1001311)

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 10 Dec 2021 15:13:59 +0200

certmonger (0.79.14+git20211010-2) unstable; urgency=medium

  * control, rules: Build-depend on systemd instead of libsystemd-dev, and
    fix service file install.
  * rules: Enable tests but don't fail the build if they fail.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 11 Oct 2021 17:00:48 +0300

certmonger (0.79.14+git20211010-1) unstable; urgency=medium

  * New upstream snapshot.
    - support OpenSSL 3.0.0 (Closes: #995641)
  * Revert-Don-t-close-STDERR-when-submitting-request.patch: Dropped,
    not needed anymore.
  * control: Build with libcurl4-openssl-dev.
  * rules: Add gentarball target.
  * fix-apache-path.diff: Dropped, obsolete.
  * source: Use 3.0 (quilt).

 -- Timo Aaltonen <tjaalton@debian.org>  Sun, 10 Oct 2021 19:38:04 +0300

certmonger (0.79.13-3) unstable; urgency=medium

  * postrm: Remove more files on purge.
  * postinst: Don't try to remove certmaster if it's not available.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 03 Nov 2020 10:47:06 +0200

certmonger (0.79.13-2) unstable; urgency=medium

  * postinst: Don't fail if certmaster CA isn't found.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 02 Nov 2020 12:36:41 +0200

certmonger (0.79.13-1) unstable; urgency=medium

  * New upstream release.
  * control, postinst: Drop certmaster, build-depend on libjansson-dev
    instead of xmlrpc.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 28 Oct 2020 16:26:33 +0200

certmonger (0.79.11-1) unstable; urgency=medium

  * New upstream release.
  * rules: Use /run instead of /var/run.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 30 Jul 2020 15:20:43 +0300

certmonger (0.79.9-2) unstable; urgency=medium

  * Revert-Don-t-close-STDERR-when-submitting-request.patch: Revert a
    commit which for some reason causes server install to fail.
    (Closes: #955530)

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 10 Apr 2020 00:06:30 +0300

certmonger (0.79.9-1) unstable; urgency=medium

  * New upstream release.
  * control: Use debhelper-compat, migrate to 12.
  * control: Bump policy to 4.5.0.
  * patches: Refreshed.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 24 Mar 2020 11:05:44 +0200

certmonger (0.79.6-2) unstable; urgency=medium

  * rules: Set homedir. (Closes: #852691)
  * control: Add nss-plugin-pem to Depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 02 May 2019 15:41:32 +0300

certmonger (0.79.6-1) unstable; urgency=medium

  * New upstream release.
  * control: Update maintainer address.
  * control: Update vcs urls.
  * Bump debhelper to 11.
  * control: Build-depend on libidn2-dev.
  * rules: Migrate to dh_missing, use --fail-missing.
  * certmonger.upstart: Removed.
  * Bump policy to 4.2.1, no changes.
  * control: Set priority: optional.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 17 Oct 2018 10:45:02 +0300

certmonger (0.79.5-3) experimental; urgency=medium

  * Merge changes from upstream git to support sqlite nssdb's.
    (LP: #1747411)
  * force-utf-8.diff: Dropped, upstream.
  * fix-apache-path.diff: Use proper path to apache nssdb.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 30 Mar 2018 09:57:57 +0300

certmonger (0.79.5-2) unstable; urgency=medium

  * force-utf8.diff: Don't clear LANG/LC_* for subprocesses, and if
    they're not set, use C.UTF-8.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 21 Feb 2018 19:59:21 +0200

certmonger (0.79.5-1) unstable; urgency=medium

  * New upstream release.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 22 Jan 2018 17:45:30 +0200

certmonger (0.79.3-1) unstable; urgency=medium

  * New upstream release.
  * control, copyright, watch: Update urls.
  * patches: Drop nspr4_path, refresh others.
  * control: Migrate to OpenSSL 1.1. (Closes: #851088)
  * control, use-dbus-run-session.diff: Use dbus-run-session instead of dbus-
    launch in tests. (Closes: #836084)
  * rules: Enable parallel build.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 07 Aug 2017 17:58:14 +0300

certmonger (0.78.6-4) unstable; urgency=medium

  * control: Build with libssl1.0-dev. (Closes: #828261)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 16 Nov 2016 09:40:15 +0200

certmonger (0.78.6-3) unstable; urgency=medium

  * Set libexecdir again, the helpers really need to be all in the same
    path. Never touch this again..

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 14 Apr 2016 12:07:46 +0300

certmonger (0.78.6-2) unstable; urgency=medium

  * rules, install: Put the helpers back in multiarch libdir.
  * postrm: Clean old cert requests on remove/purge.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 06 Apr 2016 11:01:58 +0300

certmonger (0.78.6-1) unstable; urgency=medium

  * New upstream release.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 22 Feb 2016 08:11:26 +0200

certmonger (0.78.5-2) unstable; urgency=medium

  * rules, install: Install libexec stuff to /usr/lib/certmonger instead of
    under multiarch path. FreeIPA needs this so that the same path works on
    every arch.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 07 Jan 2016 21:16:02 +0200

certmonger (0.78.5-1) unstable; urgency=medium

  * New upstream release.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 09 Dec 2015 15:09:24 +0200

certmonger (0.78.4-1) unstable; urgency=medium

  * New upstream release.
  * control: Add libpopt-dev to build-depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Sun, 16 Aug 2015 11:02:04 +0300

certmonger (0.75.14-4) unstable; urgency=medium

  [ Michael Biebl ]
  * control: Transition to libsystemd, fix build-depends. (Closes: #779744)

 -- Timo Aaltonen <tjaalton@debian.org>  Sat, 11 Jul 2015 05:28:43 +0300

certmonger (0.75.14-3) unstable; urgency=medium

  * control: Depend on dbus. (Closes: #769446)

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 05 Dec 2014 10:30:43 +0200

certmonger (0.75.14-2) unstable; urgency=medium

  * control: Use libsystemd-login-dev build-dep on linux only.
  * rules: Disable tests for now until failure on mipsel is
    investigated.
  * control: Bump policy to 3.9.6, no changes.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 16 Oct 2014 12:18:13 +0300

certmonger (0.75.14-1) unstable; urgency=low

  * New upstream release. (Closes: #751637)
  * control: Bump policy to 3.9.5, no changes.
  * control, compat, .install: Bump compat to 9.
  * control, rules: Add support for systemd, install upstart job
    unconditionally.
  * rules: Add config options for hardened build.
  * control: Add libsystemd-login-dev to build-depends, sort them while
    at it.
  * install: Fix tmpfiles.d install path.
  * rules: Don't purge po/*.gmo on clean
  * control: Add libkrb5-dev to build-depends. (Closes: #747799)
  * nspr4_path: Rebased.
  * control: Add libldap-dev ja libidn11-dev to build-depends.
  * source: Use 1.0 format again due to upstream messing with
    translations.
  * control: Update my email.
  * control: Update vcs urls.
  * fix-keythi-h-path.diff: Fix configure test to find keythi.h.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 08 Oct 2014 11:28:46 +0300

certmonger (0.57-1) unstable; urgency=low

  * Initial release (Closes: #644367)

 -- Timo Aaltonen <tjaalton@ubuntu.com>  Fri, 29 Jun 2012 18:41:18 +0200
